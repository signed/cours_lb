#!/usr/bin/env python3

from hashlib import sha256
import time
from copy import deepcopy

import pandas as pd

class cBlock:
    def __init__(self, data) -> None:
        self.data = data
        self.numBloc = 0
        self.prevHash = ""
        self.currHash = ""
        self.nonce = 0
    
    def miner(self):
        for nonce in range(1, 10**1000):
            sha = sha256()
            sha.update(str(self.numBloc).encode())
            sha.update(str(self.data).encode())
            sha.update(str(self.prevHash).encode())
            sha.update(str(nonce).encode())
            currHash = sha.hexdigest()
            if currHash[-3:] == "000":
                self.currHash = currHash
                self.nonce = nonce
                break
        return

class cChain():
    def __init__(self) -> None:
        self.listeBlocks = []

    def creerGenesis(self):
        data = ""
        genesis = cBlock(data)
        genesis.numBloc = 1
        sha = sha256()
        sha.update(str(data).encode())
        sha.update(str(genesis.nonce).encode())
        genesis.currHash = sha.hexdigest()
        genesis.nonce += 1
        self.listeBlocks.append(genesis)

    def ajouterBloc(self, data):
        block = cBlock(data)
        block.prevHash = self.listeBlocks[-1].currHash
        block.numBloc = self.listeBlocks[-1].numBloc + 1
        block.miner()
        self.listeBlocks.append(block)

    def afficherLongueurChaine(self):
        print(self.listeBlocks[-1].numBloc)
    
    def verifierChaine(self):
        return self.listeBlocks[-1].currHash[-3:] == "000"

    def afficherChaine(self):
        for bloc in self.listeBlocks:
            print({"numBloc": bloc.numBloc, "data": bloc.data, "prevHash": bloc.prevHash, "currHash": bloc.currHash, "nonce": bloc.nonce})

    def recupChaine(self):
        returnBlock = []
        for bloc in self.listeBlocks:
            returnBlock.append({"numBloc": bloc.numBloc, "data": bloc.data, "prevHash": bloc.prevHash, "currHash": bloc.currHash, "nonce": bloc.nonce})
        return returnBlock

def main():
    bc = cChain()
    bc.creerGenesis()
    bc.ajouterBloc("2")
    print(bc.verifierChaine())
    bc.ajouterBloc("89")
    print(bc.verifierChaine())
    bc.afficherChaine()


if __name__ == "__main__":
    main()