#!/usr/bin/env python3

import random

class cJoueur:
    def __init__(self, nom) -> None:
        self.nom = nom
        self.listeValeurs = []
    
    def LancerLesDes(self):
        self.listeValeurs = [random.randint(1,6), random.randint(1,6), random.randint(1,6)]

    def verifier(self):
        status = [self.verifier421(), self.verifier2as(),  self.verifierBaraque()]
        if any(status):
            if status.index(True) == 0:
                print("421")
                return 11
            elif status.index(True) == 1:
                print("Baraque")
                return 2
            elif status.index(True) == 2:
                print("2as")
                return 1
            else:
                return -1
        else:
            return -1

    def verifier421(self):
        return sorted(self.listeValeurs) == [1,2,4]

    def verifierBaraque(self):
        return len(set(self.listeValeurs)) <= 1

    def verifier2as(self):
        return self.listeValeurs.count(1) == 2

class cJeu(cJoueur):
    def __init__(self, nom) -> None:
        cJoueur.__init__(self, nom)
    
    def start(self, play_count):
        count = 0
        for _ in range(play_count):
            self.LancerLesDes()
            if self.verifier():
                count += 1
            


def main():
    joueur = cJeu("signed")



if __name__ == "__main__":
    main()